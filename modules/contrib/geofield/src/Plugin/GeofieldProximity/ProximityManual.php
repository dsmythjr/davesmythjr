<?php

namespace Drupal\geofield\Plugin\GeofieldProximity;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\geocoder\Geocoder;
use Drupal\geocoder\ProviderPluginManager;
use Drupal\views\Plugin\views\ViewsHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Default backend for Geofield.
 *
 * @GeofieldProximity(
 *   id = "geofield_manual_filter",
 *   admin_label = @Translation("Manual (Geocoded Location) Proximity Filter")
 * )
 */
class ProximityManual extends GeofieldProximityBase implements ContainerFactoryPluginInterface {
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('geocoder'),
      $container->get('plugin.manager.geocoder.provider')
    );
  }

  /**
   * ProximityManual constructor.
   *
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param \Drupal\geocoder\Geocoder $geocoder
   * @param \Drupal\geocoder\ProviderPluginManager $providerPluginManager
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Geocoder $geocoder, ProviderPluginManager $providerPluginManager) {
    $this->geocoder = $geocoder;
    $this->providerPluginManager = $providerPluginManager;
  }

  /**
   * @param \Drupal\views\Plugin\views\ViewsHandlerInterface $views_plugin
   * @return bool|array
   */
  public function getSourceValue(ViewsHandlerInterface $views_plugin) {
    if (isset($views_plugin->value) && isset($views_plugin->value['origin'])) {
      $target_location = $views_plugin->value['origin'];
    }
    else {
      return FALSE;
    }

    $plugins = $this->providerPluginManager->getPluginsAsOptions();
    $locations = $this->geocoder->geocode($target_location, $plugins);

    if (count($locations)) {
      // @TODO Might be nice to allow the user to pick the correct address.
      return [
        'latitude' => $locations->get(0)->getCoordinates()->getLatitude(),
        'longitude' => $locations->get(0)->getCoordinates()->getLongitude(),
      ];
    }
    else {
      // @TODO Probably want a nicer error state?
      return FALSE;
    }
  }

}
