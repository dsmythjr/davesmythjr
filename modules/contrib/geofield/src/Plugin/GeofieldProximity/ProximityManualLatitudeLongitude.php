<?php

namespace Drupal\geofield\Plugin\GeofieldProximity;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\views\Plugin\views\ViewsHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Default backend for Geofield.
 *
 * @GeofieldProximity(
 *   id = "geofield_manual_latitude_longitude_filter",
 *   admin_label = @Translation("Manual (Latitude, Longitude) Proximity Filter")
 * )
 */
class ProximityManualLatitudeLongitude extends GeofieldProximityBase implements ContainerFactoryPluginInterface {
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * @param $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\views\Plugin\views\ViewsHandlerInterface $views_plugin
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state, ViewsHandlerInterface $views_plugin) {
    parent::buildOptionsForm($form, $form_state, $views_plugin);

    // Add note about how to provide origin.
    if (isset($form['value'])){
      $form['value']['#suffix'] = '<div class="description" >' . t('Provide the latitude and longitude of the origin point separated by a comma.') . '</div></div>';
    }
  }

  /**
   * @param $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\views\Plugin\views\ViewsHandlerInterface $views_plugin
   */
  public function validateOptionsForm(&$form, FormStateInterface $form_state, ViewsHandlerInterface $views_plugin) {
    parent::validateOptionsForm($form, $form_state, $views_plugin);

    // Validate the cooridates are valid latitude and longitude.
    $options = $form_state->getValue('options');
    if (!empty($options['value']['origin'])) {
      if (!$this::validateLatitudeLongitude($options['value']['origin'])) {
        $form_state->setErrorByName('options', t('Invalid coordinates provided.'));
      }
    }
  }

  /**
   * @param \Drupal\views\Plugin\views\ViewsHandlerInterface $views_plugin
   * @return bool|array
   */
  public function getSourceValue(ViewsHandlerInterface $views_plugin) {
    if (isset($views_plugin->value) && isset($views_plugin->value['origin'])) {
      $target_location = $views_plugin->value['origin'];
    }
    // Retrieve source location from filter. This is needed when using plugin from field or sort.
    elseif (isset($views_plugin->field) && isset($views_plugin->view->filter[$views_plugin->field])) {
      $target_location = $views_plugin->view->filter[$views_plugin->field]->value['origin'];
    }
    else{
      return FALSE;
    }

    if (!$this::validateLatitudeLongitude($target_location)) {
      return FALSE;
    }

    list($latitude, $longitude) = $this::splitLatitudeLongitude($target_location);
    return [
      'latitude' => $latitude,
      'longitude' => $longitude,
    ];
  }

  /**
   * Split coordinate string into an array of latitude and longitude.
   *
   * @param string $latitudeLongitude
   * @return array
   */
  public static function splitLatitudeLongitude($latitudeLongitude) {
    list($latitude, $longitude) = explode(',', $latitudeLongitude);
    return [(float) $latitude, (float) $longitude];
  }

  /**
   * Validates if Latitude and Longitude provide as string are valid.
   *
   * @param string $latitudeLongitude
   * @return bool
   */
  private static function validateLatitudeLongitude($latitudeLongitude) {
    if (empty($latitudeLongitude)) {
      return FALSE;
    }
    list($latitude, $longitude) = explode(',', $latitudeLongitude);
    if (!is_numeric($latitude) || !is_numeric($longitude)) {
      return FALSE;
    }
    if ($longitude < -180 || $longitude > 180) {
      return FALSE;
    }
    if ($latitude < -90 || $latitude > 90) {
      return FALSE;
    }
    return TRUE;
  }

}
