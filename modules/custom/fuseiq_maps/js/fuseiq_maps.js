(function($) {
	var map;
	var markers = [];

	function GetLocation(distance) {
		var geocoder = new google.maps.Geocoder();
		var address = document.getElementById("map-google-search").value;
		geocoder.geocode({
			'address': address
		}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				var latitude = results[0].geometry.location.lat();
				var longitude = results[0].geometry.location.lng();
				var lat_long = latitude + "," + longitude;
				var filtered_json_url = "/rest/view/locations/all";
				deleteMarkers();
				buildMap(filtered_json_url, latitude, longitude, distance);
			} else {
				alert("Please Enter an Address.")
			}
		});
	};

	function GetLocationByZIP(zip) {
		deleteMarkers();
		var zip = document.getElementById("map-search-field-zip").value;
		var filtered_json_url = "/rest/view/locations/" + zip;
		buildMap(filtered_json_url);
	};

	function initialize() {
		var input = document.getElementById('map-google-search');
		var autocomplete = new google.maps.places.Autocomplete(input, {
			types: ['geocode']
		});
		//autocomplete.addListener('place_changed', getFormValues);
	}
	if (typeof google != "undefined") {
		google.maps.event.addDomListener(window, 'load', initialize);
	}

	function loadingMessage() {
		$('#loading-message').show();
		$('#loading-message').html('<h3 style="text-align: center">Loading Results</h3><br><div class="bar"></div>');
	}

	function removeMessage() {
		$('.loading-message').hide();
	}
	// Functions to get search or filtering values and construct the JSON URL

	function getFormValues() {
		//Get Filter Values
		var distance_value = document.getElementById('distance-options').value;
		var search_value = document.getElementById('map-google-search').value;
		if ((distance_value == 'any') && (search_value.length > 3)) {
			distance_value = 'all'
			var filtered_json_url = "/rest/view/locations/" + distance_value;
			GetLocation('all');
			return;
		}
		if (distance_value != 'any') {
			var filtered_json_url = "/rest/view/locations/" + distance_value;
			GetLocation(distance_value);
			return;
		}
		if ((distance_value == 'any') && (search_value.length < 3)) {
			distance_value = 'all'
			var filtered_json_url = "/rest/view/locations/" + distance_value;
			buildMap(filtered_json_url);
			return;
		}
	}
	$('#map-search-submit').on('click', function() {
		event.preventDefault();
		loadingMessage();
		deleteMarkers();
		getFormValues();
	});

	function initMap() {
		var styles = [{
			"featureType": "landscape.natural.terrain",
			"stylers": [{
				"hue": "#ff006e"
			}, {
				"saturation": 23
			}]
		}, {
			'featureType': "administrative",
			'elementType': "geometry.fill",
			'stylers': [{
				'visibility': "off"
			}]
		}, {
			"featureType": "landscape",
			"elementType": "all",
			"stylers": [{
				"color": "#f2f2f2"
			}]
		}, {
			"featureType": "poi",
			"elementType": "all",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "road",
			"elementType": "all",
			"stylers": [{
				"saturation": -100
			}, {
				"lightness": 45
			}]
		}, {
			"featureType": "road.highway",
			"elementType": "all",
			"stylers": [{
				"visibility": "simplified"
			}]
		}, {
			"featureType": "road.arterial",
			"elementType": "labels.icon",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "transit",
			"elementType": "all",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "water",
			"elementType": "all",
			"stylers": [{
				"color": "#117dc2"
			}, {
				"visibility": "on"
			}]
		}];
		var mapCanvas = document.getElementById('map-canvas');
		var mapOptions = {
			zoom: 15,
			maxZoom: 15,
			zoomControl: true,
			//draggable: false,
			disableDefaultUI: true,
			mapTypeId: google.maps.MapTypeId.ROAD
		};
		map = new google.maps.Map(mapCanvas, mapOptions);
		map.setOptions({
			styles: styles
		});
		var infoWindow = new google.maps.InfoWindow();
	}

	function addMarker(location) {
		var marker = new google.maps.Marker({
			position: location,
			map: map
		});
	}
	// Sets the map on all markers in the array.

	function setMapOnAll(map) {
		for (var i = 0; i < markers.length; i++) {
			markers[i].setMap(map);
		};
	}
	// Removes the markers from the map, but keeps them in the array.

	function clearMarkers() {
		setMapOnAll(null);
	}
	// Shows any markers currently in the array.

	function showMarkers() {
		setMapOnAll(map);
	}
	// Deletes all markers in the array by removing references to them.

	function deleteMarkers() {
		clearMarkers();
		markers = [];
	}
	// Location Finder
	$('body').ready(function() {
		$("#location_results_list").empty();
		return false;
	});

	function GetUrlValue(VarSearch) {
		var SearchString = window.location.search.substring(1);
		var VariableArray = SearchString.split('&');
		for (var i = 0; i < VariableArray.length; i++) {
			var KeyValuePair = VariableArray[i].split('=');
			if (KeyValuePair[0] == VarSearch) {
				return KeyValuePair[1];
			}
		}
	}
	// Retrieve location value from URL
	var selected_loc = GetUrlValue('location');
	if (typeof selected_loc == 'undefined') {
		var selected_loc = 'all';
		$('.location-reset').hide();
	}
	var drupal_view_url = "/rest/view/locations/" + selected_loc;

	function calcDistance(fromLat, fromLng, toLat, toLng) {
		return google.maps.geometry.spherical.computeDistanceBetween(
		new google.maps.LatLng(fromLat, fromLng), new google.maps.LatLng(toLat, toLng)) * 0.000621371192;
	}
	initMap();
	getFormValues();
	removeMessage();
	// create info window outside of each - then tell that singular infowindow to swap content based on click
	var infowindow = new google.maps.InfoWindow({
		content: ''
	});
	/* 
	 * Add map marker, infobox and add click event
	 */

	function add_marker(results_array, i, map, infowContent) {
		// Get Latitude/Longitude 
		var lat_lng = new google.maps.LatLng(results_array[i][1], results_array[i][2])
		// Get map boundries
		var bounds = new google.maps.LatLngBounds();
		bounds.extend(new google.maps.LatLng(results_array[i][1], results_array[i][2]));
		// Create marker
		var marker = new google.maps.Marker({
			position: lat_lng,
			animation: google.maps.Animation.DROP,
			thing: i,
			title: results_array[i][0],
			map: map
		});
		// add marker to array
		markers.push(marker);
		map.markers.push(marker);
		// show info window when marker is clicked & close other markers
		google.maps.event.addListener(marker, 'click', function() {
			//swap content of that singular infowindow
			map.setCenter(marker.getPosition()); /*     				map.setZoom( 9 ); */
			infowindow.setContent(infowContent);
			infowindow.open(map, marker);
			onMarkerClick(this.thing);
		});
		// close info window when map is clicked
		google.maps.event.addListener(map, 'click', function(event) {
			if (infowindow) {
				infowindow.close();
			}
		});
		// Remove class on POI list item on infowindow close
		google.maps.event.addListener(infowindow, 'closeclick', function() {
			$('.list-element').removeClass('active');
		});
	}
	// Function to get non-geolocated map

	function buildMap(url, lat, lon, distance = 'all') {
		markers = [];
		// Get the json feed based on the selected filters
		$.ajax({
			url: url + '?_format=hal_json',
			type: 'get',
			dataType: 'json',
			headers: {
        'Content-Type':'application/json'
      },
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				console.log('Unable to get the JSON feed');
			},
			success: function(data) {
				var bounds = new google.maps.LatLngBounds();
				
				// Fired when the map becomes idle after panning or zooming.
				google.maps.event.addListener(map, 'idle', function() {
					showVisibleMarkers(data, lat, lon, distance);
				});
				// Placing List besides map
				if (data.length != 0) {
					//console.log(data.nodes);
					$("#location_results_list").html("");
					showVisibleMarkers(data);
					var results_array = [];
					i = 0;
					$.each(data, function(index, obj) {
						miles = calcDistance(obj.lat, obj.lon, lat, lon);
						//console.log(distance);
						if (distance == 'all') {
							results_array.push([obj.title, obj.lat, obj.lon, obj.path, obj.nid, obj.distance, i]);
						} else {
							if (miles <= distance) {
								results_array.push([obj.title, obj.lat, obj.lon, obj.path, obj.nid, obj.distance, i]);
							}
						}
						// Adding letters to be used as labels and references to click to open markers on map
						i++;
					});
				}
				//var bounds = new google.maps.LatLngBounds();
				// Create markers from results_array
				if (results_array.length > 0) {
					$('.loading-message').hide();
					$('.map-container-wrapper').show();
					markers = [];
					if (markers != 'undefined') {
						map.markers = [];
					}
					showVisibleMarkers(data, lat, lon, distance);
					$('#geo_locate_message').html('<div></div>');
					for (var i = 0; i < results_array.length; i++) {
						var content_string = '<div class="marker-info-window">' + '<a style="color: #000;" class="large-marker-title" href="' + results_array[i][3] + '">' + results_array[i][0] + '</a>' + '<p><a href="https://www.google.com/maps?daddr=' + results_array[i][1] + ',' + results_array[i][2] + '" target="_blank" class="window-directions" style="color: #000;">Get Directions</a></p></div>';
						var infowindow = new google.maps.InfoWindow({
							content: content_string
						});
						var letter = String.fromCharCode("a".charCodeAt(0) + i);
						var count = (1 + i);
						/* 
						 * Add map marker, infobox and add click event
						 */
						add_marker(results_array, i, map, infowindow.content);
					}
					// Fit the markers on the map
				} else {
					//console.log('No Results');
					$('.map-container-wrapper').hide();
					removeMessage();
					$('.loading-message').show();
					$('.loading-message').html("<div style='text-align: center'><h2>No Results to Show</h2><br><h3>Please Try Different Search Criteria</h3></div>");
				}
				// loop through all markers and create bounds no matter mode
				$.each(map.markers, function(i, marker) {
					var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
					bounds.extend(latlng);
				});
				map.fitBounds(bounds);
				// Add a click event handler to each of the legend links
				for (var i = 0; i < map.markers.length; i++) {
					$('#p' + i).click(function() {
						var index = parseInt($(this).attr('data-map-id'));
						var marker = map.markers[index];
						google.maps.event.trigger(markers[index], 'click');
						return false;
					});
				}
			}
		});
		return;
	}
	// Function to scroll the legend link box for the proper item
	$.fn.scrollTo = function(elem, speed) {
		$(this).animate({
			scrollTop: $(this).scrollTop() - $(this).offset().top + $(elem).offset().top
		}, speed == undefined ? 1000 : speed);
		return this;
	};

	function onMarkerClick(tt) {
		$('.list-element').removeClass('active');
		$('#p' + tt).addClass('active');
		$(".locator").scrollTo('#p' + tt, 1000);
	}
	// Utility empty check

	function empty(data) {
		if (typeof(data) == 'number' || typeof(data) == 'boolean') {
			return false;
		}
		if (typeof(data) == 'undefined' || data === null) {
			return true;
		}
		if (typeof(data.length) != 'undefined') {
			return data.length == 0;
		}
		var count = 0;
		for (var i in data) {
			if (data.hasOwnProperty(i)) {
				count++;
			}
		}
		return count == 0;
	}

	function showVisibleMarkers(data, lat, lon, distance) {
		var bounds = map.getBounds();
		var count = 0;
		// Use the sort function after the JSON feed has been acquired
		$('#loading-message').hide();
		if (!$('body.html').hasClass('map-processed')) {
			// Clearing out the list before populating results from page load or application of new filters  
			$("#location_results_list").html("");
			// For each node result, add it to the list.
			var results_array = [];
			var i = 0;
			$.each(data, function(index, obj) {
				//console.log(obj);
				miles = calcDistance(obj.lat, obj.lon, lat, lon);
				if (distance == 'all') {
					results_array.push([obj.title, obj.field_address, obj.lat, obj.lon, obj.path, obj.nid, obj.distance, i]);
				} else {
					if (miles <= distance) {
						results_array.push([obj.title, obj.field_address, obj.lat, obj.lon, obj.path, obj.nid, obj.distance, i]);
					}
				}
				var poilist = '';
				//var poilist = createPOIList(obj.node.poi_title, obj.node.poi_link);
				// Format the html of the list of results
				var miles_away = "";
				//if (obj.node.distance != 0) {
				//var miles_away = ' - ' +  obj.node.distance + ' Miles Away';
				//}
				i++;
			});
			html = '';
			for (var i = 0; i < results_array.length; i++) {
				// Adding letters to be used as labels and references to click to open markers on map
				var letter = String.fromCharCode("A".charCodeAt(0) + i);
				//var count = (1 + i);
				$("#location_results_list").append($('<li class="info-' + (i + 1) + '"><span class="list-element" id="p' + i + '" data-map-id="' + i + '">' + '<h4 class="title-wrapper"><a class="map-loc-title"> ' + results_array[i][0] + '</a></h4><div class="address-wrapper">' + results_array[i][1] + '</div></span></li>', {
					"html": html
				}));
				//i++;
			}
			$('body.html').addClass('map-processed');
		}
		// Add click event to results list and trigger marker click event
		var c = 0;
		var allMarkers = map.markers;
		if (allMarkers != undefined) {
			allMarkers.forEach(function(item) {
				$('#p' + c).click(function() {
					map.setCenter(item.getPosition());
					google.maps.event.trigger(item, 'click');
					return false;
				});
				c++;
				var marker = item;
				var infoPanel = $('.info-' + (c)); // array indexes start at zero, but not our class names :)
				if (bounds.contains(marker.getPosition()) === true) {
					infoPanel.show();
				} else {
					infoPanel.hide();
				}
			});
		}
	}
})(jQuery);