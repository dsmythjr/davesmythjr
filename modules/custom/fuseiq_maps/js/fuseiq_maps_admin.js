(function ($) {

    $('#edit-finder').click(function(e) {
	    e.preventDefault();
	    codeAddress();
    });
    
            function initialize() {
          var input = document.getElementById('edit-geoautocomplete');
          var autocomplete = new google.maps.places.Autocomplete(input);
          autocomplete.addListener('place_changed', codeAddress);
        }
        
        google.maps.event.addDomListener(window, 'load', initialize);         


    
   function codeAddress() {
	var geocoder = new google.maps.Geocoder();
    //var address = document.getElementById("edit-field-address-0-address-address-line1").value;
    //var city = document.getElementById("edit-field-address-0-address-locality").value;
    //var state = document.getElementById("edit-field-address-0-address-administrative-area").value;
    //var zip = document.getElementById("edit-field-address-0-address-postal-code").value;
    var address = document.getElementById("edit-geoautocomplete").value;
    //var address_string = address + ' ' + city + ' ' + state + ' ' + zip;
    
    geocoder.geocode({ 'address': address }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
	        var address_components = results[0].address_components;
			var components={}; 
			$.each(address_components, function(k,v1) {
				$.each(v1.types, function(k2, v2){
					console.log(v1);
					if((v1.types[0]=='country') || (v1.types[0]=='administrative_area_level_1')) {
						components[v2]=v1.short_name
					} else {
						components[v2]=v1.long_name
					}
				});
			});
			console.log(components);
            var latitude = results[0].geometry.location.lat();
            var longitude = results[0].geometry.location.lng();
            var lat_long = latitude + "," + longitude;
            console.log(results[0]);
            document.getElementById("edit-field-geofield-0-value-lat").value = latitude;
            document.getElementById("edit-field-geofield-0-value-lon").value = longitude;
            document.getElementById("edit-field-address-0-address-address-line1").value = components.street_number + ' ' + components.route;
            document.getElementById("edit-field-address-0-address-locality").value = components.locality;
            document.getElementById("edit-field-address-0-address-administrative-area").value = components.administrative_area_level_1;
            document.getElementById("edit-field-address-0-address-postal-code").value = components.postal_code;
            //document.getElementById("edit-field-address-0-address-country-code--2").value = components.country;
        } else {
            alert("Request failed.")
        }
    });
    
  }      
       




})(jQuery);