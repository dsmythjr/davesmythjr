<?php

namespace Drupal\fuseiq_maps\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Navigation' Block.
 *
 * @Block(
 *   id = "fuseiq_maps",
 *   admin_label = @Translation("FuseIQ Custom Map"),
 * )
 */
class FuseIQMap extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }
     
  public function build() {
	  

	  $query = db_query("select distinct field_address_locality from node__field_address");
	  $records = $query->fetchAll();
	  $city_list = '<select id="map-search-field-zip">';
	  $city_list .= '<option value="all">All</option>';
	  foreach ($records as $row) {
		  $city = $row->field_address_locality;
	      $city_list .= '<option value="' . $city . '">' . $city . '</option>';
      }   
      $city_list .= '</select>';
      $places_form = '<input type="text" name="autocomplete" id="map-google-search">';
      $distance = '<select id="distance-options"><option value="any">Any Distance</option><option value="1">1 Mile</option><option value="5">5 Miles</option><option value="10">10 Miles</option><option value="20">20 Miles</option><option value="50">50 Miles</option></select>';
      return array(
        '#title' => 'FuseIQ Map',
        '#theme' => 'fuseiq_maps_mu',
        '#city_var' => $city_list,
        '#places_var' => $places_form,
        '#distance_var' => $distance,
        '#attached' => array(
          'library' => array(
            'fuseiq_maps/googlemaps',
            'fuseiq_maps/fuseiq-maps',
          ),
        ),        
      ); 
    }
}
 