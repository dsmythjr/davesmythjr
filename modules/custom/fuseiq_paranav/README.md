To use the fuseiq Paragraph Navigator:
--
There must be a paragraphs field with a machine name of field_page_paragraphs - If you wish to change this, edit the ParaNav.php file src/Plugin/Block
--
You must add a Text field with Machine name of field_menu_title on each paragraph that you want to create a menu link for
-Hide this field from the display of the paragraph
--
There is a block created from this module called 'fuseiq Paragraph Nav'. Place this in a region that makes sense to your design.
--
When editing a page with paragraphs on it, you will enter the 'Menu Title' in the field you created on each paragraph.
--
The order of the menu items will be the order the paragraphs show on the page.
--
After you change the order of parapraphs, or change the titles, you much cleat the cache /admin/config/development/performance
--
You can change the offset and animation, etc in the JS file
--
If you make the block in a sticky header, you will also have an 'active' class for each menu item when the correlating paragraph comes in view. '