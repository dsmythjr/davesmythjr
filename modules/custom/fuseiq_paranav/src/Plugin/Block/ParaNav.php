<?php

namespace Drupal\fuseiq_paranav\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Navigation' Block.
 *
 * @Block(
 *   id = "fuseiq_paranav",
 *   admin_label = @Translation("fuseiq Paragraph Nav"),
 * )
 */
class ParaNav extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }
     
  public function build() {
    $lang_code = \Drupal::service('language_manager')->getCurrentLanguage()->getId();
    $node = \Drupal::routeMatch()->getParameter('node');
    if ($node->hasField('field_page_paragraphs')) {    
      $paras = $node->get('field_page_paragraphs')->referencedEntities();
      $items = '<ul id="top-menu">';
      foreach ($paras as $para) {
        // Remember to check if translation exists
        if ($para->hasTranslation($lang_code)) {
          $para = $para->getTranslation($lang_code);
        }
        if ($para->hasField('field_menu_title')) {
          $id_array = $para->get('id')->getValue();
          $id = $id_array[0]['value'];
         $goodValue = $para->get('field_menu_title')->getValue();
         if(!empty($goodValue)) {
          $items .= '<li><a href="#paragraph-' . $id . '">' . $goodValue[0]['value'] . '</a></li>';
         }
        }
        //dpm($goodValue[0]['value']);
      }
      $items .= '</ul>';
      
      return array(
        '#markup' => $this->t($items),
        '#attached' => array(
          'library' => array(
            'fuseiq_paranav/fuseiq-paranav',
          ),
        ),        
      ); 
    }
  }
}