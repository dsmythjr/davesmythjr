<?php

namespace Drupal\fuseiq_paranav\Controller;

use Drupal\Core\Controller\ControllerBase;

class ParaNavController extends ControllerBase {

  /**
   * Display the markup.
   *
   * @return array
   */
  public function content() {
    return array(
      '#type' => 'markup',
    );
  }

}